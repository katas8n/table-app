import { oneOfType, string, number, bool, shape, oneOf } from 'prop-types';

const TableCell = ({ children, head, colspan, style, type }) => {
  const CellTag = head ? 'th' : 'td';

  return (
    <CellTag colSpan={colspan} style={style}>
      {children}
    </CellTag>
  );
};

TableCell.propTypes = {
  children: oneOfType([string, number]),
  head: bool,
  colspan: oneOfType([string, number]),
  style: shape({ background: string, color: string }),
  type: oneOf(['DATE', 'NUMBER', 'MONEY', 'TEXT']),
};

TableCell.defaultProps = {
  children: 'text',
  colspan: '2',
  style: {
    color: 'blue',
    background: 'green',
  },
};

export default TableCell;
