import TableCell from '../TableCell';

const TableRow = ({ cells, head }) => {
  return (
    <tr>
      {cells.map((cell, index) => (
        <TableCell key={index} {...cell} head={head}>
          {cell.text}
        </TableCell>
      ))}
    </tr>
  );
};

export default TableRow;
