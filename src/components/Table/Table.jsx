import { arrayOf, shape, string } from 'prop-types';
import TableRow from '../TableRow';

const tableData = [
  [{ text: 'Name' }, { text: 'Surname' }],
  [{ text: 'Jhon' }, { text: 'Doe' }],
  [{ text: 'Jhon' }, { text: 'Smith' }],
];

const Table = ({ data }) => {
  return (
    <table>
      <thead>
        <TableRow cells={data[0]} head />
      </thead>
      <tbody>
        {data.slice(1).map((row, index) => (
          <TableRow key={index} cells={row} />
        ))}
      </tbody>
    </table>
  );
};

Table.propTypes = {
  data: arrayOf(
    arrayOf(
      shape({
        text: string,
      })
    )
  ),
};

Table.defaultProps = {
  data: tableData,
};

export default Table;
